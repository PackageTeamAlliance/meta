<?php

namespace Pta\Meta\Contracts\Seo;

interface SeoEntityContract
{
    /**
     * Returns the entity that belongs to this seo entry.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    public function entity();

    /**
     * Accessor for the "meta" attribute.
     *
     * @param  string  $meta
     * @return array
     */
    public function getMetaAttribute($meta);

    /**
     * Mutator for the "meta" attribute.
     *
     * @param  array  $meta
     * @return void
     */
    public function setMetaAttribute(array $meta);
}
