<?php
namespace Pta\Meta\Http\Controllers\Frontend;

use Pta\Meta\Http\Controllers\Controller;

class SeosController extends Controller
{
    
    /**
     * Return the main view.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        return view('pta/meta::index');
    }
}
