<?php

namespace Pta\Meta\Http\Controllers\Admin;

use Pta\Meta\Http\Controllers\Controller;
use Pta\Meta\Repositories\Seo\SeoRepositoryInterface;

class seoController extends Controller
{
    
    /**
     * The Meta repository.
     *
     * @var \Ninjaparade\Meta\Repositories\Seo\SeoRepositoryInterface
     */
    protected $seo;
    
    /**
     * Constructor.
     *
     * @param  \Ninjaparade\Meta\Repositories\Seo\SeoRepositoryInterface  $seo
     * @return void
     */
    public function __construct(SeoRepositoryInterface $seo) {
        
        $this->seo = $seo;
    }
    
    /**
     * Display a listing of seo.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        return view('pta/meta::seo.index');
    }
    
    /**
     * Show the form for creating new seo.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return $this->showForm('create');
    }
    
    /**
     * Handle posting of the form for creating new seo.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store() {
        return $this->processForm('create');
    }
    
    /**
     * Show the form for updating seo.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id) {
        return $this->showForm('update', $id);
    }
    
    /**
     * Handle posting of the form for updating seo.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id) {
        return $this->processForm('update', $id);
    }
    
    /**
     * Remove the specified seo.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        $type = $this->seo->delete($id) ? 'success' : 'error';
        
        $this->alerts->{$type}(trans("pta/meta::seo/message.{$type}.delete"));
        
        return redirect()->route('admin.pta.meta.seo.all');
    }
    
    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int  $id
     * @return mixed
     */
    protected function showForm($mode, $id = null) {
        
        // Do we have a seo identifier?
        if (isset($id)) {
            if (!$seo = $this->seo->find($id)) {
                $this->alerts->error(trans('pta/meta::seo/message.not_found', compact('id')));
                
                return redirect()->route('admin.pta.meta.seo.all');
            }
        } 
        else {
            $seo = $this->seo->createModel();
        }
        
        // Show the page
        return view('pta/meta::seo.form', compact('mode', 'seo'));
    }
    
    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null) {
        
        // Store the seo
        list($messages) = $this->seo->store($id, request()->all());
        
        // Do we have any errors?
        if ($messages->isEmpty()) {
            $this->alerts->success(trans("pta/meta::seo/message.success.{$mode}"));
            
            return redirect()->route('admin.pta.meta.seo.all');
        }
        
        $this->alerts->error($messages, 'form');
        
        return redirect()->back()->withInput();
    }
}
