<?php

namespace Pta\Meta\Http\Controllers\Admin;

use Pta\Meta\Http\Controllers\Controller;
use Pta\Meta\Repositories\Sitemap\SitemapRepositoryInterface;

class SitemapsController extends Controller
{
    
    /**
     * The Meta repository.
     *
     * @var \pta\Meta\Repositories\Sitemap\SitemapRepositoryInterface
     */
    protected $sitemaps;
    
    /**
     * Constructor.
     *
     * @param  \pta\Meta\Repositories\Sitemap\SitemapRepositoryInterface  $sitemaps
     * @return void
     */
    public function __construct(SitemapRepositoryInterface $sitemaps) {
        
        $this->sitemaps = $sitemaps;
    }
    
    /**
     * Display a listing of sitemap.
     *
     * @return \Illuminate\View\View
     */
    public function index() {
        return view('pta/meta::sitemaps.index');
    }
    
    /**
     * Show the form for creating new sitemap.
     *
     * @return \Illuminate\View\View
     */
    public function create() {
        return $this->showForm('create');
    }
    
    /**
     * Handle posting of the form for creating new sitemap.
     *
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store() {
        return $this->processForm('create');
    }
    
    /**
     * Show the form for updating sitemap.
     *
     * @param  int  $id
     * @return mixed
     */
    public function edit($id) {
        return $this->showForm('update', $id);
    }
    
    /**
     * Handle posting of the form for updating sitemap.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update($id) {
        return $this->processForm('update', $id);
    }
    
    /**
     * Remove the specified sitemap.
     *
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete($id) {
        $type = $this->sitemaps->delete($id) ? 'success' : 'error';
        
        $this->alerts->{$type}(trans("pta/meta::sitemaps/message.{$type}.delete"));
        
        return redirect()->route('admin.pta.meta.sitemaps.all');
    }
    
    /**
     * Shows the form.
     *
     * @param  string  $mode
     * @param  int  $id
     * @return mixed
     */
    protected function showForm($mode, $id = null) {
        
        // Do we have a sitemap identifier?
        if (isset($id)) {
            if (!$sitemap = $this->sitemaps->find($id)) {
                $this->alerts->error(trans('pta/meta::sitemaps/message.not_found', compact('id')));
                
                return redirect()->route('admin.pta.meta.sitemaps.all');
            }
        } 
        else {
            $sitemap = $this->sitemaps->createModel();
        }
        
        // Show the page
        return view('pta/meta::sitemaps.form', compact('mode', 'sitemap'));
    }
    
    /**
     * Processes the form.
     *
     * @param  string  $mode
     * @param  int  $id
     * @return \Illuminate\Http\RedirectResponse
     */
    protected function processForm($mode, $id = null) {
        
        // Store the sitemap
        list($messages) = $this->sitemaps->store($id, request()->all());
        
        // Do we have any errors?
        if ($messages->isEmpty()) {
            $this->alerts->success(trans("pta/meta::sitemaps/message.success.{$mode}"));
            
            return redirect()->route('admin.pta.meta.sitemaps.all');
        }
        
        $this->alerts->error($messages, 'form');
        
        return redirect()->back()->withInput();
    }
}
