<?php namespace Pta\Meta\Providers;

use Cartalyst\Support\ServiceProvider;

class SitemapServiceProvider extends ServiceProvider
{
    
    /**
     * {@inheritDoc}
     */
    public function boot() {
        
        // Subscribe the registered event handler
        $this->app['events']->subscribe('pta.meta.sitemap.handler.event');
    }
    
    /**
     * {@inheritDoc}
     */
    public function register() {
        
        // Register the repository
        $this->bindIf('pta.meta.sitemap', 'Pta\Meta\Repositories\Sitemap\SitemapRepository');
        
        // Register the data handler
        $this->bindIf('pta.meta.sitemap.handler.data', 'Pta\Meta\Handlers\Sitemap\SitemapDataHandler');
        
        // Register the event handler
        $this->bindIf('pta.meta.sitemap.handler.event', 'Pta\Meta\Handlers\Sitemap\SitemapEventHandler');
        
        // Register the validator
        $this->bindIf('pta.meta.sitemap.validator', 'Pta\Meta\Validator\Sitemap\SitemapValidator');
    }
}
