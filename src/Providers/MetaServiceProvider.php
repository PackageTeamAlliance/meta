<?php
namespace Pta\Meta\Providers;

use Illuminate\Support\ServiceProvider;
use Pta\Meta\Providers\SitemapServiceProvider;
use Pta\Meta\Providers\SeoServiceProvider;

class MetaServiceProvider extends ServiceProvider
{
    
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;
    
    /**
     * Providers to register
     *
     * @var array
     */
    protected $providers = [];
    
    /**
     * Boot the application events.
     *
     * @return void
     */
    public function boot() {
        
        $this->registerBindings();
        
        $this->registerConfig();
        
        $this->registerViews();
        
        $this->registerTranslations();
        
        $this->registerMigration();
    }
    
    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register() {
        
        foreach ($this->providers as $provider) {
            
            $this->app->register($provider);
        }
    }
    
    /**
     * Register Bindings in IoC.
     *
     * @return void
     */
    protected function registerBindings() {
    }
    
    /**
     * Register config.
     *
     * @return void
     */
    protected function registerConfig() {
        
        $this->publishes([realpath(__DIR__ . '/../../config/config.php') => config_path('meta.php'), 'config']);
        
        $this->mergeConfigFrom(realpath(__DIR__ . '/../../config/config.php'), 'meta');
    }
    
    /**
     * Register views.
     *
     * @return void
     */
    public function registerViews() {
        
        $this->loadViewsFrom(realpath(__DIR__ . '/../../resources/views'), 'pta/meta');
        
        $this->publishes([realpath(__DIR__ . '/../../resources/views') => base_path('resources/views/vendor/pta/meta'), ]);
    }
    
    /**
     * Register translations.
     *
     * @return void
     */
    public function registerTranslations() {
   
        $this->loadTranslationsFrom(realpath(__DIR__ . '/../../resources/lang'), 'pta/meta');
    }
    
    public function registerMigration() {
        
        $this->publishes([realpath(__DIR__ . '/../../database/migrations') => database_path('/migrations') ], 'migrations');
    }
    
  
    public function registerRoutes() {
        
        // $router = $this->app['router'];
        
        // $prefix = $this->app['config']->get('pta/pages.route_prefix', 'dashboard');
        
        // $security = $this->app['config']->get('pta/pages.security.protected', true);
        
        // if (!$this->app->routesAreCached()) {
            
        //     $group = [];
            
        //     $group['prefix'] = $prefix;
            
        //     if ($security) {
                
        //         $middleware = $this->app['config']->get('pta/pages.security.middleware', ['auth', 'needsPermission']);
        //         $permissions = $this->app['config']->get('pta/pages.security.permission_name');
                
        //         $group['middleware'] = $this->app['config']->get('pta/pages.security.middleware', $middleware);
        //         $group['can'] = $this->app['config']->get('pta/pages.security.permission_name', $permissions);
        //     }
            
        //     $router->group($group, function () use ($router) {
                
        //         require realpath(__DIR__ . '/../routes.php');
        //     });
            
        //     $group = [];
            
        //     $group['namespace'] = \Pta\Pages\Http\Controllers\Frontend::class;
            
        //     $pages = $this->app['pta.pages']->findAll();
            
        //     $router->group($group, function () use ($router, $pages) {
                
        //         foreach ($pages as $page) {
                    
        //             $router->get($page->uri, 'PagesController@index');
                    
        //             foreach ($page->translation as $translation) {
        //                 $router->get($translation->uri, 'PagesController@index');
        //             }
        //         }
        //     });
        // }
    }    
}
