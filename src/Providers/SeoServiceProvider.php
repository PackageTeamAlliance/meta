<?php namespace Pta\Meta\Providers;

use Cartalyst\Support\ServiceProvider;

class SeoServiceProvider extends ServiceProvider {

	/**
	 * {@inheritDoc}
	 */
	public function boot()
	{
		// Subscribe the registered event handler
		$this->app['events']->subscribe('pta.meta.seo.handler.event');
	}

	/**
	 * {@inheritDoc}
	 */
	public function register()
	{
		// Register the repository
		$this->bindIf('pta.meta.seo', 'Pta\Meta\Repositories\Seo\SeoRepository');

		// Register the data handler
		$this->bindIf('pta.meta.seo.handler.data', 'Pta\Meta\Handlers\Seo\SeoDataHandler');

		// Register the event handler
		$this->bindIf('pta.meta.seo.handler.event', 'Pta\Meta\Handlers\Seo\SeoEventHandler');

		// Register the validator
		$this->bindIf('pta.meta.seo.validator', 'Pta\Meta\Validator\Seo\SeoValidator');
	}

}
