<?php namespace Pta\Meta\Models;

use Illuminate\Database\Eloquent\Model;

class Sitemap extends Model implements EntityInterface {


	/**
	 * {@inheritDoc}
	 */
	protected $table = 'sitemaps';

	/**
	 * {@inheritDoc}
	 */
	protected $guarded = [
		'id'
	];

	/**
	 * {@inheritDoc}
	 */
	protected $with = [
		
	];
}
