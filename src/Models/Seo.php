<?php

namespace Pta\Meta\Models;

use Illuminate\Database\Eloquent\Model;
use Pta\Meta\Contracts\Seo\SeoEntityContract;

class Seo extends Model implements SeoEntityContract
{
    
    /**
     * {@inheritDoc}
     */
    protected $table = 'seo';
    
    /**
     * {@inheritDoc}
     */
    protected $fillable = ['meta', 'title', 'description', 'keywords', 'robots'];
    
    /**
     * {@inheritdoc}
     */
    public function entity() {
        return $this->morphTo();
    }
    
    /**
     * {@inheritdoc}
     */
    public function getMetaAttribute($meta) {
        return $meta ? collect(json_decode($meta, true)) : [];
    }
    
    /**
     * {@inheritdoc}
     */
    public function setMetaAttribute(array $meta) {
        $this->attributes['meta'] = $meta ? json_encode($meta) : '';
    }
    
    public static function getModel(array $data = []) {
        $class = '\\' . ltrim('Pta\Meta\Models\Seo', '\\');
        
        return new $class($data);
    }
}
