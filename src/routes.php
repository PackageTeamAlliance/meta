<?php

// $router->group(['namespace' => 'Ninjaparade\Meta\Http\Controllers\Admin'], function () use ($router) {
    
//     $router->get('/', ['as' => 'meta.dashboard.index', 'uses' => 'PagesController@index']);

// 	$router->get('create' , ['as' => 'pages.dashboard.create', 'uses' => 'PagesController@create']);
// 	$router->post('create', ['as' => 'pages.dashboard.create.process', 'uses' => 'PagesController@store']);
	
// 	$router->get('copy/{id}' , ['as' => 'pages.dashboard.copy', 'uses' => 'PagesController@copy']);
// 	$router->post('copy/{id}', ['as' => 'pages.dashboard.copy.process', 'uses' => 'PagesController@store']);

// 	$router->get('{id}/edit'   , ['as' => 'pages.dashboard.edit'  , 'uses' => 'PagesController@edit']);
// 	$router->post('{id}/edit'  , ['as' => 'pages.dashboard.edit.process'  , 'uses' => 'PagesController@update']);

// 	$router->get('{id}/translate'   , ['as' => 'pages.dashboard.translate'  , 'uses' => 'PagesController@translate']);
// 	$router->post('{id}/translate'  , ['as' => 'pages.dashboard.translate.process'  , 'uses' => 'PagesController@translate_store']);

// 	$router->get('edit/{page_id}/translate/{id}'  , ['as' =>  'pages.dashboard.translate.edit'  , 'uses' => 'PagesController@translate_edit']);
// 	$router->post('edit/{page_id}/translate/{id}'  , ['as' => 'pages.dashboard.translate.edit.process'  , 'uses' => 'PagesController@translate_edit_process']);
	
// 	$router->get('{id}/delete',   ['as' => 'pages.dashboard.delete', 'uses' => 'PagesController@delete']);
// });