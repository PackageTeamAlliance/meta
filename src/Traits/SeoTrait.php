<?php 

namespace Pta\Meta\Traits;


trait SeoTrait {

	 /**
     * Returns the seo entry that belongs to entity.
     *
     * @return \Illuminate\Database\Eloquent\Relations\MorphTo
     */
    abstract public function seo();

     /**
     * Determines if the entity has a seo entry attached.
     *
     * @return bool
     */
    public function hasSeo()
    {
        return (bool) $this->seo;
    }

     /**
     * Creates a new seo entry
     *
     * @param  array  $attributes
     * @return Seo
     */
    public function createSeo(array $data)
    {
        return $this->seo()->create($data);
    }

     /**
     * Updates the seo entry with the given attributes.
     *
     * @param  array  $attributes
     * @return \Werxe\LaravelSeo\Contracts\Seo
     */
    public function updateSeo(array $data)
    {
        $seo = $this->seo;

        $seo->fill($data)->save();

        return $seo;
    }

    /**
     * Creates or Updates the seo entry with the given attributes.
     *
     * @param  array  $attributes
     * @return Seo
     */
    public function storeSeo(array $attributes)
    {
        $method = ! $this->seo ? 'createSeo' : 'updateSeo';

        return $this->{$method}($attributes);
    }

    /**
     * Deletes the seo entry that's attached to the entity.
     *
     * @return Seo
     */
    public function deleteSeo()
    {
        return $this->seo->delete();
    }
}