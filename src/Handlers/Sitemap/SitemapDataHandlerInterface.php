<?php

namespace Pta\Meta\Handlers\Sitemap;

interface SitemapDataHandlerInterface {
    
    /**
     * Prepares the given data for being stored.
     *
     * @param  array  $data
     * @return mixed
     */
    public function prepare(array $data);
}
