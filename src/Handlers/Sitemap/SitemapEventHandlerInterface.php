<?php namespace Ninjaparade\Meta\Handlers\Sitemap;

use Ninjaparade\Meta\Models\Sitemap;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface SitemapEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a sitemap is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a sitemap is created.
	 *
	 * @param  \Ninjaparade\Meta\Models\Sitemap  $sitemap
	 * @return mixed
	 */
	public function created(Sitemap $sitemap);

	/**
	 * When a sitemap is being updated.
	 *
	 * @param  \Ninjaparade\Meta\Models\Sitemap  $sitemap
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Sitemap $sitemap, array $data);

	/**
	 * When a sitemap is updated.
	 *
	 * @param  \Ninjaparade\Meta\Models\Sitemap  $sitemap
	 * @return mixed
	 */
	public function updated(Sitemap $sitemap);

	/**
	 * When a sitemap is deleted.
	 *
	 * @param  \Ninjaparade\Meta\Models\Sitemap  $sitemap
	 * @return mixed
	 */
	public function deleted(Sitemap $sitemap);

}
