<?php

namespace Pta\Meta\Handlers\Sitemap;

use Illuminate\Events\Dispatcher;
use Pta\Meta\Models\Sitemap;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class SitemapEventHandler extends BaseEventHandler implements SitemapEventHandlerInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function subscribe(Dispatcher $dispatcher) {
        $dispatcher->listen('pta.meta.sitemap.creating', __CLASS__ . '@creating');
        $dispatcher->listen('pta.meta.sitemap.created', __CLASS__ . '@created');
        $dispatcher->listen('pta.meta.sitemap.updating', __CLASS__ . '@updating');
        $dispatcher->listen('pta.meta.sitemap.updated', __CLASS__ . '@updated');
        $dispatcher->listen('pta.meta.sitemap.deleted', __CLASS__ . '@deleted');
    }
    
    /**
     * {@inheritDoc}
     */
    public function creating(array $data) {
    }
    
    /**
     * {@inheritDoc}
     */
    public function created(Sitemap $sitemap) {
        $this->flushCache($sitemap);
    }
    
    /**
     * {@inheritDoc}
     */
    public function updating(Sitemap $sitemap, array $data) {
    }
    
    /**
     * {@inheritDoc}
     */
    public function updated(Sitemap $sitemap) {
        $this->flushCache($sitemap);
    }
    
    /**
     * {@inheritDoc}
     */
    public function deleted(Sitemap $sitemap) {
        $this->flushCache($sitemap);
    }
    
    /**
     * Flush the cache.
     *
     * @param  \Ninjaparade\Meta\Models\Sitemap  $sitemap
     * @return void
     */
    protected function flushCache(Sitemap $sitemap) {
        $this->app['cache']->forget('pta.meta.sitemap.all');   
        $this->app['cache']->forget('pta.meta.sitemap.' . $sitemap->id);
    }
}
