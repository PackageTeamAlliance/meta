<?php namespace Pta\Meta\Handlers\Seo;

use Pta\Meta\Models\Seo;
use Cartalyst\Support\Handlers\EventHandlerInterface as BaseEventHandlerInterface;

interface SeoEventHandlerInterface extends BaseEventHandlerInterface {

	/**
	 * When a seo is being created.
	 *
	 * @param  array  $data
	 * @return mixed
	 */
	public function creating(array $data);

	/**
	 * When a seo is created.
	 *
	 * @param  \Ninjaparade\Meta\Models\Seo  $seo
	 * @return mixed
	 */
	public function created(Seo $seo);

	/**
	 * When a seo is being updated.
	 *
	 * @param  \Ninjaparade\Meta\Models\Seo  $seo
	 * @param  array  $data
	 * @return mixed
	 */
	public function updating(Seo $seo, array $data);

	/**
	 * When a seo is updated.
	 *
	 * @param  \Ninjaparade\Meta\Models\Seo  $seo
	 * @return mixed
	 */
	public function updated(Seo $seo);

	/**
	 * When a seo is deleted.
	 *
	 * @param  \Ninjaparade\Meta\Models\Seo  $seo
	 * @return mixed
	 */
	public function deleted(Seo $seo);

}
