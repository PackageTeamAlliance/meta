<?php

namespace Pta\Meta\Handlers\Seo;

interface SeoDataHandlerInterface {
    
    /**
     * Prepares the given data for being stored.
     *
     * @param  array  $data
     * @return mixed
     */
    public function prepare(array $data);
}
