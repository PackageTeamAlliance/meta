<?php

namespace Pta\Meta\Handlers\Seo;

use Illuminate\Events\Dispatcher;
use Pta\Meta\Models\Seo;
use Cartalyst\Support\Handlers\EventHandler as BaseEventHandler;

class SeoEventHandler extends BaseEventHandler implements SeoEventHandlerInterface
{
    
    /**
     * {@inheritDoc}
     */
    public function subscribe(Dispatcher $dispatcher) {
        $dispatcher->listen('pta.meta.seo.creating', __CLASS__ . '@creating');
        $dispatcher->listen('pta.meta.seo.created', __CLASS__ . '@created');
        $dispatcher->listen('pta.meta.seo.updating', __CLASS__ . '@updating');
        $dispatcher->listen('pta.meta.seo.updated', __CLASS__ . '@updated');
        $dispatcher->listen('pta.meta.seo.deleted', __CLASS__ . '@deleted');
    }
    
    /**
     * {@inheritDoc}
     */
    public function creating(array $data) {
    }
    
    /**
     * {@inheritDoc}
     */
    public function created(Seo $seo) {
        $this->flushCache($seo);
    }
    
    /**
     * {@inheritDoc}
     */
    public function updating(Seo $seo, array $data) {
    }
    
    /**
     * {@inheritDoc}
     */
    public function updated(Seo $seo) {
        $this->flushCache($seo);
    }
    
    /**
     * {@inheritDoc}
     */
    public function deleted(Seo $seo) {
        $this->flushCache($seo);
    }
    
    /**
     * Flush the cache.
     *
     * @param  \Pta\Meta\Models\Seo  $seo
     * @return void
     */
    protected function flushCache(Seo $seo) {
        $this->app['cache']->forget('pta.meta.seo.all');
        
        $this->app['cache']->forget('pta.meta.seo.' . $seo->id);
    }
}
