<?php

namespace Pta\Meta\Console\Installers;

use Illuminate\Console\Command;
use Pta\Pulse\Console\Contracts\Installer;

class MetaInstaller implements Installer
{
    
    /**
     * @var Command
     */
    protected $command;
    
    public function run(Command $command) {
        
        $this->command = $command;
        
        $this->command->call('module:migrate', ['module' => 'meta']);
    }
}
