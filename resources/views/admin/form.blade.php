
<div style="padding-top:40px;"></div>
{!! BootForm::text(trans('pta/meta::seo/model.general.title'), 'title')->value(old('title', $seo->title))->attribute('id', 'title') !!}

{!! BootForm::text(trans('pta/meta::seo/model.general.description'), 'description')->value(old('description', $seo->description))->attribute('id', 'description') !!}

{!! BootForm::text(trans('pta/meta::seo/model.general.keywords'), 'keywords')->value(old('keywords', $seo->keywords))->attribute('id', 'keywords') !!}


{!! BootForm::select( trans('pta/meta::seo/model.general.robots'), 'robots')
	->options(
		[
			'index,follow' => 'index,follow', 
			'index,nofollow' => 'index,nofollow',
			'noindex,follow' => 'noindex,follow',
			'noindex,nofollow' => 'noindex,nofollow',
		]
	)
	->attribute('id', 'robots') 
	->select(old('robots', $seo->robots));
!!}



