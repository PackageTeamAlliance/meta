<?php

return [

	'general' => [

		'id' => 'Id',
		'title' => 'Title',
		'description' => 'Description',
		'keywords' => 'Keywords',
		'robots' => 'Robots',
		'created_at' => 'Created At',
		'title_help' => 'Enter the Title here',
		'description_help' => 'Enter the Description here',
		'keywords_help' => 'Enter the Keywords here',
		'robots_help' => 'Enter the Robots here',

	],

];
