<?php

return [

	// General messages
	'not_found' => 'Seo [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Seo was successfully created.',
		'update' => 'Seo was successfully updated.',
		'delete' => 'Seo was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the seo. Please try again.',
		'update' => 'There was an issue updating the seo. Please try again.',
		'delete' => 'There was an issue deleting the seo. Please try again.',
	],

];
