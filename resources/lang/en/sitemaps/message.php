<?php

return [

	// General messages
	'not_found' => 'Sitemap [:id] does not exist.',

	// Success messages
	'success' => [
		'create' => 'Sitemap was successfully created.',
		'update' => 'Sitemap was successfully updated.',
		'delete' => 'Sitemap was successfully deleted.',
	],

	// Error messages
	'error' => [
		'create' => 'There was an issue creating the sitemap. Please try again.',
		'update' => 'There was an issue updating the sitemap. Please try again.',
		'delete' => 'There was an issue deleting the sitemap. Please try again.',
	],

];
