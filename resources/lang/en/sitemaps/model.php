<?php

return [

	'general' => [

		'id' => 'Id',
		'url' => 'Url',
		'lastmod' => 'Lastmod',
		'changefreq' => 'Changefreq',
		'priority' => 'Priority',
		'created_at' => 'Created At',
		'url_help' => 'Enter the Url here',
		'lastmod_help' => 'Enter the Lastmod here',
		'changefreq_help' => 'Enter the Changefreq here',
		'priority_help' => 'Enter the Priority here',

	],

];
