<?php

return [

	'index'  => 'List Sitemaps',
	'create' => 'Create new Sitemap',
	'edit'   => 'Edit Sitemap',
	'delete' => 'Delete Sitemap',

];
