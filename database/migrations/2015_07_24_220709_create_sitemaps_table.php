<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateSitemapsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('sitemaps', function(Blueprint $table)
		{
			$table->increments('id');
			$table->string('url');
			$table->dateTime('lastmod');
			$table->dateTime('changefreq');
			$table->float('priority');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('sitemaps');
	}

}
