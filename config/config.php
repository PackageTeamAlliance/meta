<?php

return [
	
	'name' => 'Meta',
	

     /*
     * Default prefix to the dashboard.
     */
    'route_prefix' => config('core.admin_uri') . 'meta',

    
    /*
     * Default permission user should have to access the dashboard.
     */
    'security'     => [
        'protected'          => true,
        'middleware'         => ['auth','needsPermission'],
        'permission_name'    => 'meta.dashboard.manage',
    ],


    'permissions' => [
        [
            'name'          => 'meta.dashboard.manage',
            'readable_name' => 'Manage Page Meta',
        ]
    ],

    'navigation' => [
        [
            'menu_id'  => -1,
            'order'    => 0,
            'dropdown' => 1,
            'submenu'  => 0,
            'active'   => 1,
            'name'     => 'Meta',
            'icon'     => '',
            'route'    => '',
            'title'    => 'Meta',
            'roles'    => -1,
            'children' => [
                [
                    'menu_id'  => -1,
                    'order'    => 2,
                    'dropdown' => 0,
                    'submenu'  => 1,
                    'parent'   => -1,
                    'active'   => 1,
                    'icon'     => '',
                    'header'   => '',
                    'route'    => 'meta.dashboard.index',
                    'name'     => 'All Meta',
                    'title'    => 'All Meta',
                    'roles'    => -1,
                ],
                [
                    'menu_id'  => -1,
                    'order'    => 1,
                    'dropdown' => 0,
                    'submenu'  => 1,
                    'parent'   => -1,
                    'active'   => 1,
                    'icon'     => '',
                    'header'   => '',
                    'route'    => 'meta.dashboard.create',
                    'name'     => 'Create Meta',
                    'title'    => 'Create Meta',
                    'roles'    => -1,
                ]
            
            ]
        ]

    ],
    /*
     * Default url used to redirect user to front/admin of your the system.
     */
    'system_url'   => config('core.redirect_url'),

];
